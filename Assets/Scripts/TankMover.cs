﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMover : MonoBehaviour 
{
	[HideInInspector]
	public Transform tf;
	private CharacterController cc;
	private TankData data;

	// Use this for initialization
	void Start () 
	{
		cc = GetComponent<CharacterController> ();
		data = GetComponent<TankData> ();
		tf = GetComponent<Transform> ();
	}

	// Function that moves the tank
	public void Move (Vector3 tankMovement)
	{
		// Makes the tank move in the direction of tankMovement and speed from TankData
		cc.SimpleMove (tankMovement.normalized * data.moveSpeed);
	}

	public void Rotate ( float turnSpeed )
	{
		data.mover.tf.Rotate (new Vector3 (0, turnSpeed * Time.deltaTime, 0));
	}

	public void RotateTo (Vector3 newDirection)
	{
		// Variable to hold how we want to be turned
		Quaternion goalRotation;
		// Set variable to how to turn to look at newDirection
		goalRotation = Quaternion.LookRotation (newDirection);
		// Rotate from current rotation towards our target rotation
		data.mover.tf.rotation = Quaternion.RotateTowards (data.mover.tf.rotation, goalRotation, data.turnSpeed * Time.deltaTime);
	}
}
