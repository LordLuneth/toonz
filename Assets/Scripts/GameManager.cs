﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour 
{
	public static GameManager gm;

	public PlayerController player;

	// Use this for initialization
	void Awake () 
	{
		if (gm == null) 
		{
			gm = this;
			DontDestroyOnLoad (gameObject);
		} 
		else 
		{
			Destroy (gameObject);
		}
	}
}
