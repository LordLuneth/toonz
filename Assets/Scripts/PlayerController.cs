﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
	// Default set keys for tank movement
	[Header("Keys")]
	public KeyCode forwardKey = KeyCode.W;
	public KeyCode backwardsKey = KeyCode.S;
	public KeyCode rotateLeftKey = KeyCode.A;
	public KeyCode rotateRightKey = KeyCode.D;
	public KeyCode shootKey = KeyCode.Space;

	private TankData data;

	// Use this for initialization
	void Start () 
	{
		data = GetComponent<TankData> ();
		// Sets THIS object as the player
		GameManager.gm.player = this;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 moveVector = Vector3.zero;

		if (Input.GetKey (forwardKey)) 
		{
			data.mover.Move (data.mover.tf.forward);
			//SendMessage ("Move", data.moveSpeed);
		} 
		else if (Input.GetKey (backwardsKey)) 
		{
			data.mover.Move (-data.mover.tf.forward);
			//SendMessage ("Move", -data.moveSpeed);
		}
		if (Input.GetKey (rotateLeftKey)) 
		{
			SendMessage ("Rotate", -data.turnSpeed);
		}
		else if (Input.GetKey (rotateRightKey)) 
		{
			SendMessage ("Rotate", data.turnSpeed);
		}

		SendMessage ("Move", moveVector, SendMessageOptions.DontRequireReceiver);

	}
}
