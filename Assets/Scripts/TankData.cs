﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankMover))]
[RequireComponent(typeof(TankShooter))]

public class TankData : MonoBehaviour 
{
	[HideInInspector]
	public PlayerController control;

	// Other Tank Components
	[HideInInspector]
	public TankMover mover;
	[HideInInspector]
	public TankShooter shooter;

	// Variables for movement speeds of tanks
	[Header("Tank Movement")]
	public float moveSpeed = 20;
	public float turnSpeed = 100;

	// Variables for tank combat
	[Header("Combat")]
	public float damageDealt = 20;
	public float tankHealth = 100;
	public float maxTankHealth = 100;
	public float scoreValue = 100;

	public void Start()
	{
		control = GetComponent<PlayerController> ();
		mover = GetComponent<TankMover> ();
	}
}
